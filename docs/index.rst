asyncjsonrpc documentation
==========================

asyncjsonrpc is an asynchronous JSON-RPC client/server module for Python.

Server usage example::

   import asyncjsonrpc
   from asyncjsonrpc.server.aiohttp_websocket_server import AiohttpWebsocketServer
   from aiohttp import web

   methods = asyncjsonrpc.MethodGroup()

   @methods.method
   def greet(name):
      return f'Hello {name}!'

   rpcserver = AiohttpWebsocketServer(methods)
   app = web.Application()
   app.add_routes([web.get('/', rpcserver)])
   web.run_app(app)

Client usage example::

   import asyncjsonrpc, asyncio
   from asyncjsonrpc.client.aiohttp_websocket_client import AiohttpWebsocketClient
   from aiohttp import web

   async def main():
      client = asyncjsonrpc.client.AiohttpWebsocketClient()
      await client.connect('http://localhost:8080/')

      print(await client.greet('world'))

   asyncio.run(main())

See the examples directory of the module's repository for more examples.


Client Classes:

* :py:class:`~asyncjsonrpc.client.aiohttp_websocket_client.AiohttpWebsocketClient`


Server Classes:

* :py:class:`~asyncjsonrpc.server.aiohttp_websocket_server.AiohttpWebsocketServer`


Common Classes:

* :py:class:`~asyncjsonrpc.request.Request`
* :py:class:`~asyncjsonrpc.response.Response`
* :py:class:`~asyncjsonrpc.method_group.MethodGroup`
* :py:class:`~asyncjsonrpc.exceptions.RpcError`