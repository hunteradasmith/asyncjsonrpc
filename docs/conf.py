import os
import sys
sys.path.insert(0, os.path.abspath('..'))


# -- Project information -----------------------------------------------------

project = 'asyncjsonrpc'
copyright = '2020, Hunter Smith'
author = 'Hunter Smith'


# -- General configuration ---------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon'
]

exclude_patterns = ['_build']


# -- Options for HTML output -------------------------------------------------

html_theme = 'alabaster'