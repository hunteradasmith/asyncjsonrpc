import asyncjsonrpc, asyncio
from asyncjsonrpc.client.aiohttp_websocket_client import AiohttpWebsocketClient
from aiohttp import web

async def main():
    client = asyncjsonrpc.client.AiohttpWebsocketClient()
    await client.connect('http://localhost:8080/')

    print(await client.greet('world'))

asyncio.run(main())