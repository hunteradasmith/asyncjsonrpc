import asyncjsonrpc
from asyncjsonrpc.server.aiohttp_websocket_server import AiohttpWebsocketServer
from aiohttp import web

methods = asyncjsonrpc.MethodGroup()

@methods.method
def greet(name):
    return f'Hello {name}!'

rpcserver = AiohttpWebsocketServer(methods)
app = web.Application()
app.add_routes([web.get('/', rpcserver)])
web.run_app(app)