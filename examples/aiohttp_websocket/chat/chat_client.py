import asyncjsonrpc
import asyncio
import sys

from asyncjsonrpc.client.aiohttp_websocket_client import AiohttpWebsocketClient
from aiohttp import web
from datetime import datetime, timedelta
from tkinter import Tk, Frame, Scrollbar, Listbox, StringVar, Entry, LEFT, RIGHT, BOTH, X, Y, END

from chat_lib import Message, MessageStore

def asyncify(coro):
    asyncio.create_task(coro)

class ChatClient:
    def __init__(self, url):
        self.message_store = MessageStore()
        self.last_sync = datetime.now() - timedelta(minutes = 30)

        self.url = url or 'http://localhost:8080/'
        self.client = None
        self.username = 'anonymous'

        self.root = self.msg_list = self.msg_var = None

        self.displayed_messages = set()
        self.message_store.add_messages(Message('system', 'Type /name <name> to set your username.'))

        self.running = True

    async def send_message(self, message):
        await self.client.send_message(message.to_list())

    async def messages_loop(self, interval = 0.1):
        while self.running:
            message_lists = await self.client.messages_since(self.last_sync.isoformat())

            if len(message_lists) > 0:
                messages = [Message(*l) for l in message_lists]
                self.message_store.add_messages(*messages)
                self.update_msg_list()

            self.last_sync = datetime.now()
            await asyncio.sleep(interval)

    def update_msg_list(self):
        for message in sorted(self.message_store.messages, key = lambda m: m.timestamp):
            if not message in self.displayed_messages:
                self.msg_list.insert(END, message.formatted)

                self.displayed_messages.add(message)

    def on_send(self, event = None):
        contents = self.msg_var.get()
        if contents.startswith('/name'):
            parts = contents.split(' ')
            if len(parts) > 1:
                self.set_username(' '.join(parts[1:]))

        else:
            message = Message(self.username, contents)
            asyncify(self.send_message(message))

        self.msg_var.set('')

    def set_username(self, username):
        message = Message(
            'system',
            f'user "{self.username}" is now known as "{username}".')

        asyncify(self.send_message(message))
        self.username = username

    def stop(self):
        self.running = False

    def build_ui(self):
        self.root = root = Tk()
        self.root.title = 'asyncjsonrpc example chat client'
        self.root.minsize(width = 800, height = 600)
        self.root.protocol("WM_DELETE_WINDOW", self.stop)

        frame = Frame(root)
        scrollbar = Scrollbar(frame)
        self.msg_list = msg_list = Listbox(frame, yscrollcommand = scrollbar)

        scrollbar.pack(side = RIGHT, fill = Y)
        msg_list.pack(side = LEFT, fill = BOTH, expand = 1)
        frame.pack(fill = BOTH, expand = 1)

        self.msg_var = msg_var = StringVar()
        entry = Entry(root, textvariable = msg_var)
        entry.bind("<Return>", self.on_send)
        entry.pack(fill = X)

    async def run_tk(self, interval = 0.05):
        # Hack to get Tk to play nice with asyncio
        # Thanks to Lucretiel on reddit:
        # https://www.reddit.com/r/Python/comments/33ecpl/neat_discovery_how_to_combine_asyncio_and_tkinter/

        while self.running:
            self.root.update()
            await asyncio.sleep(interval)

    async def main(self):
        self.client = AiohttpWebsocketClient()
        print(f'connecting to {self.url}...')
        try:
            await self.client.connect(self.url)
        except Exception as e:
            print(f'connection failed: {e.message}')
            quit()

        print('connected')

        self.build_ui()
        self.update_msg_list()

        await asyncio.gather(
            self.run_tk(),
            self.messages_loop())

        self.root.destroy()

url = sys.argv[1] if len(sys.argv) > 2 else None
print(sys.argv[1], url)
asyncio.run(ChatClient(url).main())