import asyncjsonrpc
import dateutil.parser

from asyncjsonrpc.server.aiohttp_websocket_server import AiohttpWebsocketServer
from aiohttp import web
from chat_lib import Message, MessageStore

message_store = MessageStore()
methods = asyncjsonrpc.MethodGroup()

@methods.method
def messages_since(timestr):
    timestamp = dateutil.parser.parse(timestr)
    messages = message_store.messages_since(timestamp)
    return [m.to_list() for m in messages]

@methods.method
def send_message(message_list):
    message = Message(*message_list)
    message_store.add_messages(message)
    print(message.formatted)

rpcserver = AiohttpWebsocketServer(methods)
app = web.Application()
app.add_routes([web.get('/', rpcserver)])
web.run_app(app)