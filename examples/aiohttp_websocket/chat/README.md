# asyncjsonrpc Chat Example

This example implements a simple text chat client and server.

### Requirements

* asyncio
* asyncjsonrpc
* tkinter

### Usage

Server:

    python3 chat_server.py

Client:

    python3 chat_server.py <optional URL, defaults to http://localhost:8080/>

If asyncjsonrpc is not installed in the system Python path, prefix both commands with:

    PYTHONPATH="<directory path containing asyncjsonrpc>"

Client users can type "/name <name>" in the chat textbox to set their chat handle.