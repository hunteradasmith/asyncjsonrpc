import dateutil.parser
from datetime import datetime, timedelta
from uuid import uuid4

class Message:
    def __init__(self, sender, message, timestamp = None, uuid = None):
        self.sender = sender
        self.message = message
        self.timestamp = timestamp
        self.uuid = uuid or str(uuid4())

        if self.timestamp == None:
            self.timestamp = datetime.now()
        elif type(self.timestamp) == str:
            self.timestamp = dateutil.parser.parse(self.timestamp)

    def __hash__(self):
        return hash(self.uuid)

    def __eq__(self, other):
        if not isinstance(other, Message):
            raise NotImplementedError()

        return self.uuid == other.uuid

    def to_list(self):
        return [self.sender, self.message, self.timestamp.isoformat(), self.uuid]

    @property
    def formatted(self):
        time = self.timestamp.strftime('%H:%M')
        return f'{time} {self.sender}: {self.message}'

class MessageStore:
    def __init__(self):
        self.messages = set()

    def add_messages(self, *messages):
        for message in messages:
            if not isinstance(message, Message):
                raise ValueError('Provided argument is not a Message')

            if not message in self.messages:
                self.messages.add(message)

    def messages_since(self, timestamp, offset = timedelta(seconds = 1)):
        return [m for m in self.messages if m.timestamp >= timestamp - offset]